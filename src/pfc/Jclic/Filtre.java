package pfc.Jclic;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class Filtre extends Activity {
	
	protected TextView tittle;
	protected Button boton1;
	protected Button boton2;
	protected Button boton3;
	protected Button boton4;
	protected Button boton5;
	protected TextView filtre1;
	protected TextView filtre2;
	protected TextView filtre3;
	
	static final int DIALOG_CATEGORIA_ID = 1;
	static final int DIALOG_EDAT_ID = 2;
	static final int DIALOG_IDIOMA_ID = 3;
	
	public void onCreate(Bundle savedInstanceState) {		
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.filtre);
	    
	    Bundle bund = getIntent().getExtras();
		final int jugar = bund.getInt("jugar");
	    
	    tittle = (TextView)findViewById(R.id.principal_title);
	    Typeface font = Typeface.createFromAsset(getAssets(), "gloriahallelujah.ttf");
	    tittle.setTypeface(font);
	    
	    filtre1 = (TextView)findViewById(R.id.filtre_edat);
	    filtre1.setTypeface(font);
	    
	    filtre2 = (TextView)findViewById(R.id.filtre_categoria);
	    filtre2.setTypeface(font);
	    
	    filtre3 = (TextView)findViewById(R.id.filtre_idioma);
	    filtre3.setTypeface(font);
	    
	    boton1 = (Button)findViewById(R.id.boton1);
	    boton1.setTypeface(font);
	    
	    boton2 = (Button)findViewById(R.id.boton2);
	    boton2.setTypeface(font);
	    
	    boton3 = (Button)findViewById(R.id.boton3);
	    boton3.setTypeface(font);
	    
	    boton4 = (Button)findViewById(R.id.boton4);
	    boton4.setTypeface(font);  
	    
	    boton5 = (Button)findViewById(R.id.boton5);
	    boton5.setTypeface(font); 

	    boton4.setOnClickListener(new View.OnClickListener() {

	      @Override
	      public void onClick(View view) {
	        if (jugar == 1) llamarJugar();
	        else llamarMasClics();
	      }

	    });
	    
	    boton5.setOnClickListener(new View.OnClickListener() {

	    	@Override
		      public void onClick(View view) {
	    		Intent i = new Intent(getApplicationContext(), Principal.class);
	    		i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	    		startActivity(i);
	    		Filtre.this.finish();
		      }

		 });
	    
	    boton2.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				showDialog(DIALOG_CATEGORIA_ID);
			}
		});

		boton1.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				showDialog(DIALOG_EDAT_ID);
			}
		});

		boton3.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				showDialog(DIALOG_IDIOMA_ID);
			}
		});
	}
	
	protected Dialog onCreateDialog(int id) {
		Dialog dialog;
		switch (id) {
		case DIALOG_CATEGORIA_ID:
			dialog = crearCategoria();
			break;
		case DIALOG_EDAT_ID:
			dialog = crearEdat();
			break;
		case DIALOG_IDIOMA_ID:
			dialog = crearIdioma();
			break;
		default:
			dialog = null;
		}
		return dialog;
	}
	
	private Dialog crearCategoria() {
		final String[] items = {
				new String("Tots"),
				new String(getString(R.string.llengues)),
				new String(getString(R.string.mates)),
				new String(getString(R.string.cien_soc)),
				new String(getString(R.string.cien_exp)),
				new String(getString(R.string.musica)),
				new String(getString(R.string.plastica)),
				new String(getString(R.string.ed_fisica)),
				new String("Diversos")
		};
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Selecciona una categoria");
		builder.setItems(items, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) {
				String categoria = items[item].toString();
				boton2.setText(categoria);
			}
		});
		return builder.create();
	}

	private Dialog crearEdat() {
		final String[] items = {
				new String("Tots"),
				new String("Infantil (3-6)"),
				new String(getString(R.string.primaria) + " (6-12)"),
				new String(getString(R.string.secundaria) + " (12-16)"),
				new String("Batxillerat (16-18)")
		};
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Selecciona un rang d'edat");
		builder.setItems(items, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) {
				String edat = items[item].toString();
				boton1.setText(edat);
			}
		});
		return builder.create();
	}

	private Dialog crearIdioma() {
		final CharSequence[] items = {
				new String("Tots"),
				new String(getString(R.string.Catala)),
				new String(getString(R.string.Esp)),
				new String("English"),
				new String("German"),
				new String("French"),
				new String("Other") };
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Selecciona un idioma");
		builder.setItems(items, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) {
				String idioma = items[item].toString();
				boton3.setText(idioma);
			}
		});
		return builder.create();
	}
	
	void llamarJugar() {
		
		String edat = boton1.getText().toString();
		String categoria = boton2.getText().toString();
		String idioma = boton3.getText().toString();
		Intent intent = new Intent(this, Inici.class);
		Bundle bund = new Bundle();
		bund.putString("edat", edat);
		bund.putString("categoria", categoria);
		bund.putString("idioma", idioma);
		intent.putExtras(bund);
		startActivity(intent);
		
		/*
		Intent intent = new Intent(this, Inici.class);
		startActivity(intent);
		
		*/
	}
	
	void llamarMasClics() {
		
				
		String edat = boton1.getText().toString();
		String categoria = boton2.getText().toString();
		String idioma = boton3.getText().toString();
		Intent intent = new Intent(this, Llibreria.class);
		Bundle bund = new Bundle();
		bund.putString("edat", edat);
		bund.putString("categoria", categoria);
		bund.putString("idioma", idioma);
		intent.putExtras(bund);
		startActivity(intent);
		
		/*
		Intent intent = new Intent(this, Inici.class);
		startActivity(intent);*/
	}

	@Override
	public void onResume() 
	{ 
		super.onResume();
	}
	
	public void onPause() 
	{
		finish();
		super.onPause();
		
	}
	@Override
	public void onBackPressed() {
	
		Intent i = new Intent(getApplicationContext(), Principal.class);
		i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(i);
		Filtre.this.finish();
	}
	
}