package pfc.Jclic;

import java.io.Console;

import android.os.Bundle;
import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.ActionBar.TabListener;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

public class NuevaVista extends FragmentActivity implements TabListener {

	private static final String STATE_SELECTED_NAVIGATION_ITEM = "selected_navigation_item";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_nueva_vista);
		
		ActionBar actBar = getActionBar();
		actBar.setTitle("Todas las categor�as");
		
		//if(actBar == null) Log.i("ojete","ojete");
        actBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
 
        // For each of the sections in the app, add a tab to the action bar.
        actBar.addTab(actBar.newTab().setText("Todos los Clicks").setTabListener( this));
       	actBar.addTab(actBar.newTab().setText("Descargas").setTabListener(this));
       	actBar.addTab(actBar.newTab().setText("M�s jugados").setTabListener( this));
    };
	
    
    //TRABAJAR CON  GRIDVIEW

    
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.nueva_vista, menu);
		//createMenu(menu);
		return true;
	}
	
	 @Override
	 
	 public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
         	case R.id.menu_settings:
         		Toast.makeText(this, "Settings Option Selexted", Toast.LENGTH_SHORT).show();
	             return true;
	         default:
	             return super.onOptionsItemSelected(item);

	         }

	  

	     }
	 
	 

	    @Override
	    public void onRestoreInstanceState(Bundle savedInstanceState) {
	        if (savedInstanceState.containsKey(STATE_SELECTED_NAVIGATION_ITEM)) {
	            getActionBar().setSelectedNavigationItem(savedInstanceState.getInt(STATE_SELECTED_NAVIGATION_ITEM));
	        }
	    }
	    



	@Override
	public void onTabReselected(Tab arg0, FragmentTransaction arg1) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
		// TODO Auto-generated method stub
		if (tab.getPosition() == 0) {
    		myGames simpleListFragment = new myGames();
    		getSupportFragmentManager().beginTransaction().replace(R.id.container, simpleListFragment).commit();
    		//fragmentTransaction.attach( simpleListFragment);
    	} 
		
		else if (tab.getPosition() == 1){
			myDownloads down = new myDownloads();
			getSupportFragmentManager().beginTransaction().replace(R.id.container, down).commit();
		}
		
		else{
			myFavourites favourites = new myFavourites();
			getSupportFragmentManager().beginTransaction().replace(R.id.container,  favourites).commit();
			
		}
		
		
		
		}
		
	
		
		
	
	
	@Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(STATE_SELECTED_NAVIGATION_ITEM, getActionBar().getSelectedNavigationIndex());
    }



	@Override
	public void onTabUnselected(Tab arg0, FragmentTransaction arg1) {
		// TODO Auto-generated method stub
		
	}

	
	
	
	
	

}
