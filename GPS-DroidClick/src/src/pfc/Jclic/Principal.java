package pfc.Jclic;

import pfc.Activitats.HolePuzzle;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class Principal extends Activity {
	
	protected TextView tittle;
	protected Button boton1;
	protected Button boton2;
	
	public void onCreate(Bundle savedInstanceState) {		
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.principal);
	    
	    tittle = (TextView)findViewById(R.id.principal_title);
	    Typeface font = Typeface.createFromAsset(getAssets(), "gloriahallelujah.ttf");
	    tittle.setTypeface(font);
	    
	    boton1 = (Button)findViewById(R.id.boton1);
	    boton1.setTypeface(font);
	    
	    boton2 = (Button)findViewById(R.id.boton2);
	    boton2.setTypeface(font);  

	    boton1.setOnClickListener(new View.OnClickListener() {

		      @Override
		      public void onClick(View view) {
		    	  llamarFiltro(1);
		      }

	    });
	    
	    boton2.setOnClickListener(new View.OnClickListener() {

		      @Override
		      public void onClick(View view) {
		        llamarFiltro(0);
		      }

		});
	    
	    
	}
	
	public void llamarFiltro(int jugar) {

		Intent intent = new Intent(this, Filtre.class);
		Bundle bund = new Bundle();
		bund.putInt("jugar", jugar);
		intent.putExtras(bund);
		startActivity(intent);
	}

	@Override
	public void onResume() 
	{ 
		super.onResume();
	}
	
	public void onPause() 
	{
		//finish();
		super.onPause();
		
	}
	
	public void onBackPressed() {
		
		Intent i = new Intent();
		i.setAction(Intent.ACTION_MAIN);
		i.addCategory(Intent.CATEGORY_HOME);
		i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(i);
		this.finish();
		android.os.Process.killProcess(android.os.Process.myPid());		
		
	}
	
}
